(uiop:define-package #:consul
  (:use #:cl
	#:consul/kvs
	#:consul/session)
  (:import-from #:consul/request
		#:*agent-address*
		#:*agent-port*
		#:*default-scheme*)
  (:use-reexport #:consul/kvs
		 #:consul/session
		 #:consul/request))
