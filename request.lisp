(uiop:define-package #:consul/request
    (:use #:cl)
  (:import-from #:alexandria
		#:flatten)
  (:import-from #:drakma
		#:http-request)
  (:import-from #:flexi-streams
		#:octets-to-string)
  (:import-from #:yason
		#:parse)
  (:export #:*agent-address*
	   #:*agent-port*
	   #:*default-scheme*
	   #:consul-request
	   #:consul-request*
	   #:define-response-accessor
	   #:request-error))

(in-package :consul/request)

(defparameter *agent-address* "127.0.0.1")
(defparameter *agent-port* 8500)
(defparameter *default-scheme* "http")

(defmacro define-response-accessor (accessor-name property-name)
  `(defun ,accessor-name (ht)
     (gethash ,property-name ht)))


(define-condition request-error (error)
  ((http-status :initarg :http-status)
   (http-message :initarg :http-message))
  (:report (lambda (condition stream)
	     (with-slots (http-status http-message) condition
	       (format stream "HTTP status:~d message: ~a" http-status http-message)))))


(defun endpoint (scheme address port &rest path-fragments)
  (format nil
	  "~a://~a:~d/~{~a~^/~}"
	  scheme
	  address
	  port
	  (flatten path-fragments)))

(defun parse-consul-response (r)
  (let ((parsed-response (parse (octets-to-string r))))
    (if (and (listp parsed-response)
	     (= (length parsed-response) 1))
	(first parsed-response)
	parsed-response)))

(defun consul-request (method path-fragments
		       &key
			 (scheme *default-scheme*)
			 (address *agent-address*)
			 (port *agent-port*)
			 parameters
			 content)
  (multiple-value-bind (body status)
      (http-request (endpoint scheme address port path-fragments)
		    :method method
		    :parameters (remove nil parameters)
		    :content content)
    (if (and (= status 200)
	     body)
	(parse-consul-response body)
	(values nil status body))))

(defun consul-request* (method path-fragments
		       &key
			 (scheme *default-scheme*)
			 (address *agent-address*)
			 (port *agent-port*)
			 parameters
			 content)
  (multiple-value-bind (result status body)
      (consul-request method path-fragments
		      :scheme scheme
		      :address address
		      :port port
		      :parameters parameters
		      :content content)
    (if result
	result
	(error 'request-error :http-status status :http-message body))))
