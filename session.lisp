(uiop:define-package #:consul/session
  (:use #:cl
	#:consul/request)
  (:import-from #:yason
		#:encode-alist)
  (:export #:create-session
	   #:destroy-session
	   #:list-session
	   #:read-session))

(in-package :consul/session)


(define-response-accessor session-id "ID")


(defun session-endpoint (&rest path-fragments)
  (apply #'list "v1" "session" path-fragments))

(defun build-endpoint-parameters-content (alist)
  (with-output-to-string (s)
    (encode-alist
     (loop
	:for (k . v) :in alist
	:when v
	:collect (cons k v))
     s)))

(defun create-session (&key lock-delay node name behavior ttl)
  (let ((parameters (build-endpoint-parameters-content
		     (list (cons "LockDelay" lock-delay)
			   (cons "Node" node)
			   (cons "Name" name)
			   (cons "Behavior" behavior)
			   (cons "TTL" ttl)))))
    (session-id (consul-request* :put (session-endpoint "create") :content parameters))))

(defun destroy-session (session)
  (consul-request :put (session-endpoint "destroy" session)))

(defun read-session (session)
  (consul-request :get (session-endpoint "info" session)))

(defun list-sessions (&optional node)
  (consul-request :get
		  (if node
		      (session-endpoint "node" node)
		      (session-endpoint "list"))))

(defun renew-session (session)
  (consul-request :put (session-endpoint "renew" session)))
