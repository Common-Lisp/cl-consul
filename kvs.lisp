(uiop:define-package #:consul/kvs
  (:use #:cl
	#:consul/request)
  (:import-from #:alexandria
		#:when-let)
  (:import-from #:cl-base64
		#:base64-string-to-string)
  (:export #:*default-value-encoder*
	   #:*default-value-parser*
	   #:delete-key
	   #:delete-key
	   #:read-key
	   #:update-key))

(in-package :consul/kvs)


(defparameter *default-value-encoder* 'identity)
(defparameter *default-value-parser* 'identity)


(define-response-accessor key-value "Value")
(define-response-accessor key-modify-index "ModifyIndex")


(defun key-endpoint (&rest path-fragments)
  (apply #'list "v1" "kv" path-fragments))

(defun read-key (k &key (value-parser *default-value-parser*))
  (when-let ((response (consul-request :get (key-endpoint k))))
    (values (funcall value-parser (base64-string-to-string (key-value response)))
	    response)))

(defun read-keys (prefix &key key-names-only)
  (consul-request :get
		  (key-endpoint prefix)
		  :parameters (list (if key-names-only
					(cons "keys" nil)
					(cons "recurse" nil)))))

(defun integer-cell (k v)
  (cons k (write-to-string v)))

(defun cas-cell (index)
  (integer-cell "cas" index))

(defun flags-cell (flags)
  (integer-cell "flags" flags))

(defun lock (action session k &key value flags (value-encoder *default-value-encoder*))
  (consul-request :put (key-endpoint k)
		  :parameters (list (flags-cell flags)
				    (cons (ecase action
					    (:acquire "acquire")
					    (:release "release"))
					  session))
		  :content (funcall value-encoder value)))

(defun acquire-lock (session k &optional v)
  (lock :acquire session k :value v))

(defun release-lock (session k &optional v)
  (lock :release session k :value v))

(defun check-and-set-key (k v &key (index 0) (flags 0) (value-encoder *default-value-encoder*))
  "If INDEX is 0, Consul will only put the key if it does not already exist.
   If INDEX is non-zero, the key is only set if the index matches the ModifyIndex of that key."
  (check-type index number)
  (consul-request :put (key-endpoint k)
		  :parameters (list (cas-cell index) (flags-cell flags))
		  :content (funcall value-encoder v)))

(defun check-and-delete-key (k &optional (index 0))
  (check-type index number)
  (consul-request :delete (key-endpoint k)
		  :parameters (list (cas-cell index))))

(defun update-key (k v &key (flags 0) (value-encoder *default-value-encoder*))
  (check-type flags integer)
  (consul-request :put (key-endpoint k)
		  :parameters (list (flags-cell flags))
		  :content (funcall value-encoder v)))

(defun delete-key (k)
  (consul-request :delete (key-endpoint k)))

(defun delete-keys (k)
  (consul-request :delete (key-endpoint k)
		  :parameters (list (cons "recurse" nil))))
