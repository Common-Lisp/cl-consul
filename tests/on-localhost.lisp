(defpackage #:consul/tests
  (:use #:cl
	#:fiveam
	#:consul
	#:consul/request))

(in-package #:consul/tests)

(def-suite consul)

(in-suite consul)

(test endpoint
  (is (equal (consul/request::endpoint "http" "127.0.0.1" 8500) "http://127.0.0.1:8500/"))
  (is (equal (consul/request::endpoint "http" "127.0.0.1" 8500 "a" "b") "http://127.0.0.1:8500/a/b"))
  (is (equal (consul/request::endpoint "http" "127.0.0.1" 8500 '("a" "b")) "http://127.0.0.1:8500/a/b")))

(test key-endpoint
  (is (equal (consul/kvs::key-endpoint) (list "v1" "kv")))
  (is (equal (consul/kvs::key-endpoint "a") (list "v1" "kv" "a")))
  (is (equal (consul/kvs::key-endpoint "a" "b") (list "v1" "kv" "a" "b"))))

(test create/read-key
  (let ((k "test-string")
	(v "a-string"))
    (update-key k v)
    (is (string= (read-key k) v))
    (delete-key k)
    (is-false (read-key k))))

(test create/read-integer-key
  (let ((k "test-integer")
	(v 1))
    (update-key k v :value-encoder #'write-to-string)
    (is (= (read-key k :value-parser #'parse-integer) v))
    (delete-key k)))
