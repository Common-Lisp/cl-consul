(defsystem "cl-consul"
  :description "Common Lisp client for HashiCorp Consul"
  :author "Didier J. Devroye <didier@devroye.me>"
  :license "MIT"
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :depends-on ("cl-consul/consul")
  :in-order-to ((test-op (test-op "cl-consul/tests"))))

(defsystem "cl-consul/tests"
  :description "Unit tests for CL-CONSUL"
  :class :package-inferred-system
  :depends-on ("cl-consul/tests/on-localhost")
  :perform (test-op (o c) (uiop:symbol-call :fiveam :run-all-tests)))

(register-system-packages "cl-consul/consul" '(:consul))
(register-system-packages "cl-consul/kvs" '(:consul/kvs))
(register-system-packages "cl-consul/request" '(:consul/request))
(register-system-packages "cl-consul/session" '(:consul/session))
(register-system-packages "cl-consul/tests" '(:consul/tests))
